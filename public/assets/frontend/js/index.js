$(document).ready(function () {
    // Performing the delete tag through ajax
    $(document).on("click",".deleteTagButton",function () {
        var tagId  = $(this).data("id");
        var token  = $('input[type="hidden"]').val();
        swal({
            title: "Are you sure?",
            icon: "warning",
            buttons: true,
            dangerMode: true
        }).then((willDelete) => {
            if(willDelete) {
                $.ajax({
                    url:'/tag/'+tagId,
                    method:"delete",
                    data:{_token:token},
                    success: function (data) {
                        swal("deleted!", "Tag deleted successfully!", "success");
                        $("#tag"+tagId).fadeOut("slow");
                    },
                });
            }
        })
    });

    // Performing the delete post through ajax
    $(document).on("click",".deletePostButton",function () {
        var postId  = $(this).data("id");
        var token  = $('input[type="hidden"]').val();
        swal({
            title: "Are you sure?",
            icon: "warning",
            buttons: true,
            dangerMode: true
        }).then((willDelete) => {
            if(willDelete) {
                $.ajax({
                    url:'/post/'+postId,
                    method:"delete",
                    data:{_token:token},
                    success: function (data) {
                        swal("deleted!", "Post deleted successfully!", "success");
                        $("#post"+postId).fadeOut("slow");
                    },
                });
            }
        })
    });
});