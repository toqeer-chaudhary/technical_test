<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function home()
    {
//        return view('home');

//       fetching all posts with pagination limit of 2
        $posts = Post::orderBy("id","desc")->paginate(2);
        $tags  = Tag::all();
        return view('posts.index',compact("posts","tags"));
    }

//    public function index()
//    {
//        return view('posts.index');
//    }
}
