<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePost;
use App\Http\Requests\UpdatePost;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;

class PostController extends Controller
{
    private $posts;

    public function __construct(Post $post)
    {
        // initializing the private variable of the calls to communicate with model
        $this->posts = $post ;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // fetching all tags to show on widget box
        $tags  = Tag::all();

        return view("posts.create",compact("tags"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
//        dd($request->all());
        $post = $this->posts->StorePost($request);
        $this->posts->syncTags($request->tags,$post->id);
        return redirect()->back()->with("success","Post Created Successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($postId)
    {
        // This function will fetch post where post id = $postId
        $post        = $this->posts->findpost($postId);
        // fetching all tags to show on widget box
        $tags  = Tag::all();
        return view("posts.show",compact("post","tags"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        $tags  = Tag::all();
        return view("posts.edit",compact("post","tags"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePost $request, $id)
    {
        $this->posts->updatePost($request,$id);
        // this function will look for the tags and or replace the values in db using sync method
        $this->posts->syncTags($request->tags,$id);
        return redirect()->route("home")->with("success","Post Updated Successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($postId)
    {
        $this->posts->deletePost($postId);
    }
}
