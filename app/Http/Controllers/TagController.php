<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTag;
use App\Http\Requests\UpdateTag;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    private $tags;

    public function __construct(Tag $tag)
    {
        // initializing the private variable of the calls to communicate with model
        $this->tags = $tag ;
        // applying auth middleware
        $this->middleware("auth");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // fetching all tags to show on widget box
        $tags  = Tag::all();
//        $tags  = array();

        return view("tags.index",compact("tags"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        return view("tags.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTag $request)
    {
       $this->tags->storeTag($request->name);
       return redirect()->back()->with("success","Tag created Successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($tagId)
    {
        // This function will fetch all posts where tag id = $id
        $tag        = $this->tags->findTag($tagId);
        // fetching all posts associate with the tag
        $postsByTag = $tag->posts()->orderBy("id","desc")->paginate(2);
        // fetching all tags to show on widget box
        $tags  = Tag::all();
        return view("tags.show-post-by-tags",compact("postsByTag","tags"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($tagId)
    {
        // fetching single tag
        $tag  = $this->tags->findTag($tagId);
        // fetching all tags
        $tags = Tag::all();
        return view("tags.edit",compact("tag","tags"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTag $request, $id)
    {
        $this->tags->updateTag($request->name,$id);
        return redirect()->route("tag.index")->with("success","Tag Updated Successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($tagId)
    {
       $this->tags->destroyTag($tagId);
    }
}
