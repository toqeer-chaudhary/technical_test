<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    // a tag can belongs to many posts
    public function posts()
    {
        return $this->belongsToMany(Post::class)->withTimestamps();
    }

    // a tag must be created by one and only one user
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // function to find a tag
    public function findTag($tagId)
    {
        return self::find($tagId);
    }

    // This function will store the data in DB
    public function storeTag($tagName)
    {
        $tag = new Tag();
        $tag->name    = $tagName;
        $tag->user_id = auth()->id();
        $tag->save();
    }

    //  This function will delete the tag
    public function destroyTag($tagId)
    {
        $tag = self::findTag($tagId);
        $tag->delete();
        $tag->posts()->detach();
    }

    // This function will update the tag
    public function updateTag($tagName, $tagId)
    {
        $tag = self::findTag($tagId);
        $tag->name    = $tagName;
        $tag->user_id = auth()->id();
        $tag->update();
    }
}
