<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
//    use AuthenticableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','email','is_admin','password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // admin can create many posts
    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    // admin can create many tags
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
