<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\Types\Self_;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','description','image'
    ];

    // post must be created by one and only one user
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // post should have many tags
    public function tags()
    {
        return $this->belongsToMany(Tag::class)->withTimestamps();
    }

    // function to find a post
    public function findpost($postId)
    {
        return self::find($postId);
    }

    // This function will create post
    public function StorePost($postData)
    {
        $post               = new Post()                            ;
        $post->title        = $postData->title                      ;
        $post->user_id      = auth()->id()                          ;
        $post->description  = $postData->description                ;
        if ($postData->hasFile('image'))
        {
            $file           = $postData->file('image')              ;
            $imageName      = time().$file->getClientOriginalName() ;
            $file->move('assets/frontend/images/post', $imageName)  ;
        }
        $post->image        = $imageName                            ;
        $post->save()                                               ;
        return $post                                                ;
    }

    // This function will sync the tags with post
    public function syncTags($tags, $postId)
    {
        $post = self::findpost($postId);
        $post->tags()->sync($tags);
    }

    // This function will delete the post
    public function deletePost($postId)
    {
        $post = self::findpost($postId);
        // unlinking the image
        if (file_exists("../public/assets/frontend/images/post/$post->image")) {
            unlink("../public/assets/frontend/images/post/$post->image");
            echo 'Deleted old image';
        }
        else {
            echo 'Image file does not exist';
        }
        $post->delete();
        // detaching the tags associated to the post
        $post->tags()->detach();
    }

    // This function will update the Post
    public function updatePost($request, $id)
    {
//        dd($request->all());
        $post = self::findpost($id);
        $post->title    = $request->title;
        $post->user_id = auth()->id();
        $post->description  = $request->description                 ;
        // if the request has the image then it will add image to db else it will store the old name
        // of the image in the db
        if ($request->hasFile('image'))
        {
            $file           = $request->file('image')               ;
            $imageName      = time().$file->getClientOriginalName() ;
            $file->move('assets/frontend/images/post', $imageName)  ;
        }
        else
        {
            $imageName = $post->image                               ;
        }

        $post->image        = $imageName                            ;
        $post->update();
    }
}
