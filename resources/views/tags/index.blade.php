{{-- This page is responsible to create posts -}}

{{-- Extending the frontend layout--}}
@extends("layouts.frontend.layout")

{{-- This section is for special css files--}}
@section("styles")
    <link rel="stylesheet" type="text/css" href="{{asset("assets/frontend/plugins/select2/css/select2.min.css")}}">
@endsection

{{-- This section is for page title--}}
@section("title")
    Create Tag
@endsection

@section("main-container")
    <div class="container">
        <div class="row">
            <!-- Blog Entries Column -->
            <div class="col-sm-12">
                @if(Session::has('success'))
                    <div class="alert alert-success mt-1">{{Session::get('success')}}</div>
                @endif
                <h1 class="my-4">Create A Tag</h1>
                <form action="{{ route("tag.store") }}" method="post" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="form-group mb-2">
                        <label for="">Tag Name : </label>
                        <input type="text" class="form-control" id="" placeholder="i.e Jquery" name="name">
                        @if ($errors->has('name'))
                            <span class="text-danger" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success float-right mb-2">Create</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1> Tags List</h1>
                <table class="table table-bordered">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Created On</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($tags as $tag)
                            <tr id="tag{{$tag->id}}">
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $tag->name }}</td>
                                <td>{{ $tag->created_at->toFormattedDateString() }}</td>
                                <td>
                                    <a class='btn btn-secondary' href='{{ route("tag.edit",$tag->id) }}' data-id="{{$tag->id}}">Edit</a>
                                    <a class='deleteTagButton btn btn-dark text-white' data-id="{{$tag->id}}">Delete</a>
                                </td>
                            </tr>
                        @empty
                               <h2 class="text-center text-danger">No Tags found</h2>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

{{-- This section is for special scripts--}}
@section("script")
    <script src="{{asset("assets/frontend/plugins/select2/js/select2.min.js")}}"></script>
    <script>
        $('.js-example-basic-multiple').select2({
            placeholder: 'Assign Users'
        });
    </script>
@endsection