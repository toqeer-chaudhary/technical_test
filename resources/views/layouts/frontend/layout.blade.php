<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    {{--following yield is for the title--}}
    <title>@yield("title")</title>

    {{--Required css files for frontend--}}
    <link rel="stylesheet" type="text/css" href="{{asset("assets/frontend/plugins/bootstrap/css/bootstrap.min.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("assets/frontend/css/style.css")}}">
    @yield("styles")
    <link rel="stylesheet" type="text/css" href="{{asset("assets/frontend/css/index.css")}}">
</head>
<!-- END HEAD -->

<body>

    {{--Section for navbar--}}
    @include("includes.frontend.nav-links")

    {{--Section for main contents--}}
    @yield("main-container")

    {{-- Footer --}}
    {{--@include("includes.frontend.footer")--}}
    {{-- Required js files for frontend--}}
    <script src="{{asset("assets/frontend/plugins/jquery/jquery.min.js")}}"></script>
    <script src="{{asset("assets/frontend/plugins/bootstrap/js/bootstrap.min.js")}}"></script>
    <script src="{{asset("assets/frontend/plugins/sweetalert.min.js")}}"></script>
    @yield("script")
    <script src="{{asset("assets/frontend/js/index.js")}}"></script>

</body>
</html>
