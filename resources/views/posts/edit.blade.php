<?php
    //  creating an array of tag id's that are associated with the post
    foreach($post->tags as $tag)
    {
     // array of id
        $tagsArrayId[] = $tag->id;
    }
?>
{{-- This page is responsible to edit posts -}}

{{-- Extending the frontend layout--}}
@extends("layouts.frontend.layout")

{{-- This section is for special css files--}}
@section("styles")
    <link rel="stylesheet" type="text/css" href="{{asset("assets/frontend/plugins/select2/css/select2.min.css")}}">
@endsection

{{-- This section is for page title--}}
@section("title")
    Edit Post
@endsection

@section("main-container")
    <div class="container">
        <div class="row">
            <!-- Blog Entries Column -->
            <div class="col-md-12">
                <h1 class="my-4">Edit The Post</h1>
                <form action="{{ route("post.update",$post->id) }}" method="post" enctype="multipart/form-data" autocomplete="off">
                    {{ method_field("PUT") }}
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Post Title</label>
                        <input type="text" class="form-control" id="" placeholder="i.e Hello World"
                               value="{{ $post->title }}" name="title">
                        @if ($errors->has('title'))
                            <span class="text-danger" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <div for="exampleFormControlSelect1">Select Tags</div>
                            @foreach($tags as $tag)
                            <label class="checkbox-inline">
                                    <input type="checkbox" value="{{ $tag->id }}"
                                           name="tags[]" {{ in_array($tag->id,$tagsArrayId) ? "checked" : "" }} > {{ $tag->name }}
                            </label>
                            @endforeach
                            @if ($errors->has('tags'))
                                <div class="text-danger" role="alert">
                                    <strong>{{ $errors->first('tags') }}</strong>
                                </div>
                            @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Post Description</label>
                        <textarea class="form-control" id="" rows="3" name="description">{{ $post->description }}</textarea>
                        @if ($errors->has('description'))
                            <span class="text-danger" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlFile1">Post Image</label>
                        <input type="file" class="form-control-file" id="" name="image">
                        @if ($errors->has('image'))
                            <span class="text-danger" role="alert">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success col-sm-2 float-right mb-2">Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

{{-- This section is for special scripts--}}
@section("script")
    <script src="{{asset("assets/frontend/plugins/select2/js/select2.min.js")}}"></script>
    <script>
        $('.js-example-basic-multiple').select2({
            placeholder: 'Assign Users'
        });
    </script>
@endsection