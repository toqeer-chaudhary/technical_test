<?php
    // an array of colors
    $badgeColors = ["primary", "secondary", "success", "danger", "warning", "info", "dark"]
?>
{{-- Extending the frontend layout--}}
@extends("layouts.frontend.layout")

{{-- This page is responsible to show posts on the home page--}}

{{-- This section is for page title--}}
@section("title")
    Home
@endsection

@section("main-container")
    <div class="container">
        @if(Session::has('success'))
            <div class="alert alert-success mt-1">{{Session::get('success')}}</div>
        @endif
        <div class="row">
            <!-- Blog Entries Column -->
            <div class="{{ Auth::check() ? "col-md-8" : "col-md-12"}}">

                {{--<h1 class="my-4">Page Heading--}}
                    {{--<small>Secondary Text</small>--}}
                {{--</h1>--}}

                <!-- Blog Post -->
                 @forelse($posts as $post)
                    <div class="card mb-4 mt-4" id="post{{$post->id}}">
                            {{--if the user is logged in then the display size of image will be 750x300
                                else it will be 1050x300--}}
                             {{--http://placehold.it/{{ Auth::check() ? "750x300" : "1050x300--}}
                            <img class="card-img-top" src="{{ asset("assets/frontend/images/post/".$post->image) }}"
                                 alt="Card image cap" width="300" height="350">
                            <div class="card-body">
                                <h2 class="card-title">{{ $post->title }}</h2>
                                {{-- This will only be shown if the user is logged in--}}
                                @if(auth::check())
                                    {{-- This loop will fetch all the tags linked with the post--}}
                                    @foreach($post->tags as $tag)
                                        {{-- The badgeColors array will work from index 0 to the size of array -1 --}}
                                        <span class="badge badge-pill badge-{{ $badgeColors[mt_rand(0,sizeof($badgeColors)-1)] }}">{{ $tag->name }}</span>
                                    @endforeach
                                @endif
                                {{-- This will only be shown if the user is logged in--}}
                                @if(auth::check())
                                    <p class="card-text">{{ $post->description }}</p>
                                @endif
                                {{-- The following condition will check if the user is looged in then the button
                                will take user to the description page else it take the user to the login page--}}
                                <a href="{{ auth::check() ? "/post/$post->id": route("login") }}" class="btn btn-primary">Read More &rarr;</a>
                               @auth
                                    @if(auth()->user()->is_admin == 1)
                                        <a class='btn btn-warning pull-right text-white' href='{{ route("post.edit",$post->id) }}' data-id="{{$post->id}}">Edit</a>
                                        <a class='deletePostButton btn btn-danger pull-right text-white' data-id="{{$post->id}}">Delete</a>
                                    @endif
                                @endauth
                            </div>
                            <div class="card-footer text-muted">
                                Posted on {{ $post->created_at->toFormattedDateString() }} by
                                <a href="#">{{ ucfirst($post->user->name) }}</a>

                            </div>
                    </div>
                     @empty
                     <h1>No Posts Available</h1>
                @endforelse

                {{--<!-- Pagination -->--}}
                <div class="pagination justify-content-center mb-4">
                    {{ $posts->links() }}
                </div>

            </div>

            <!-- Sidebar Widgets Column -->
            {{-- Note this coloumn will only be shown if the user is logged in --}}
            <div class="col-md-4 {{ Auth::check() ? "" : "d-none"}}">

                <!-- Search Widget -->
                {{--<div class="card my-4">--}}
                    {{--<h5 class="card-header">Search</h5>--}}
                    {{--<div class="card-body">--}}
                        {{--<div class="input-group">--}}
                            {{--<input type="text" class="form-control" placeholder="Search for...">--}}
                            {{--<span class="input-group-btn">--}}
                  {{--<button class="btn btn-secondary" type="button">Go!</button>--}}
                {{--</span>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

            {{-- Including tags widget--}}
            @include("includes.frontend.tags-widget")

                <!-- Side Widget -->
                {{--<div class="card my-4">--}}
                    {{--<h5 class="card-header">Side Widget</h5>--}}
                    {{--<div class="card-body">--}}
                        {{--You can put anything you want inside of these side widgets. They are easy to use, and feature the new Bootstrap 4 card containers!--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
    {{-- Footer --}}
    <footer class="py-3 bg-dark bottom">
        {{--class = fixed-bottom--}}
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; Technical Test 2018</p>
        </div>
    </footer>
@endsection