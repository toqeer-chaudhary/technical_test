<?php
// an array of colors
$badgeColors = ["primary", "secondary", "success", "danger", "warning", "info", "dark"]
?>
{{-- Extending the frontend layout--}}
@extends("layouts.frontend.layout")

{{-- This page is responsible to show posts on the home page--}}

{{-- This section is for page title--}}
@section("title")
    Post Details
@endsection

@section("main-container")
    <div class="container">
        <div class="row">
            <!-- Blog Entries Column -->
            <div class="{{ Auth::check() ? "col-md-8" : "col-md-12"}}">

            {{--<h1 class="my-4">Page Heading--}}
            {{--<small>Secondary Text</small>--}}
            {{--</h1>--}}

            <!-- Blog Post -->
                    <div class="card mb-4 mt-4">
                        {{--if the user is logged in then the display size of image will be 750x300
                            else it will be 1050x300--}}
                        {{--http://placehold.it/{{ Auth::check() ? "750x300" : "1050x300--}}
                        <img class="card-img-top" src="{{ asset("assets/frontend/images/post/".$post->image) }}"
                             alt="Card image cap" width="300" height="350">
                        <div class="card-body">
                            <h2 class="card-title">{{ $post->title }}</h2>
                            {{-- This will only be shown if the user is logged in--}}
                            @if(auth::check())
                                {{-- This loop will fetch all the tags linked with the post--}}
                                @foreach($post->tags as $tag)
                                    {{-- The badgeColors array will work from index 0 to the size of array -1 --}}
                                    <span class="badge badge-pill badge-{{ $badgeColors[mt_rand(0,sizeof($badgeColors)-1)] }}">{{ $tag->name }}</span>
                                @endforeach
                            @endif
                            {{-- This will only be shown if the user is logged in--}}
                            @if(auth::check())
                                <p class="card-text">{{ $post->description }}</p>
                            @endif
                        </div>
                        <div class="card-footer text-muted">
                            Posted on {{ $post->created_at->toFormattedDateString() }} by
                            <a href="#">{{ ucfirst($post->user->name) }}</a>
                        </div>
                    </div>


                {{--<!-- Pagination -->--}}
                {{--<div class="pagination justify-content-center mb-4">--}}
                    {{--{{ $posts->links() }}--}}
                {{--</div>--}}

            </div>

            <!-- Sidebar Widgets Column -->
            {{-- Note this coloumn will only be shown if the user is logged in --}}
            <div class="col-md-4 {{ Auth::check() ? "" : "d-none"}}">

                <!-- Search Widget -->
            {{--<div class="card my-4">--}}
            {{--<h5 class="card-header">Search</h5>--}}
            {{--<div class="card-body">--}}
            {{--<div class="input-group">--}}
            {{--<input type="text" class="form-control" placeholder="Search for...">--}}
            {{--<span class="input-group-btn">--}}
            {{--<button class="btn btn-secondary" type="button">Go!</button>--}}
            {{--</span>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}

            {{-- Including tags widget--}}
            @include("includes.frontend.tags-widget")

            <!-- Side Widget -->
                {{--<div class="card my-4">--}}
                {{--<h5 class="card-header">Side Widget</h5>--}}
                {{--<div class="card-body">--}}
                {{--You can put anything you want inside of these side widgets. They are easy to use, and feature the new Bootstrap 4 card containers!--}}
                {{--</div>--}}
                {{--</div>--}}

            </div>
        </div>
    </div>
@endsection