{{-- This page is responsible to create posts -}}

{{-- Extending the frontend layout--}}
@extends("layouts.frontend.layout")

{{-- This section is for special css files--}}
@section("styles")
    <link rel="stylesheet" type="text/css" href="{{asset("assets/frontend/plugins/select2/css/select2.min.css")}}">
@endsection

{{-- This section is for page title--}}
@section("title")
    Create Post
@endsection

@section("main-container")
    <div class="container">
        <div class="row">
            <!-- Blog Entries Column -->
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="alert alert-success mt-1">{{Session::get('success')}}</div>
                @endif
             <h1 class="my-4">Create A post</h1>
                <form action="{{ route("post.store") }}" method="post" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Post Title</label>
                        <input type="text" class="form-control" id="" placeholder="i.e Hello World" name="title"
                        value="{{old("title")}}">
                        @if ($errors->has('title'))
                            <span class="text-danger" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Post Description</label>
                        <textarea class="form-control" id="" rows="3" name="description" >{{ old("description") }}</textarea>
                        @if ($errors->has('description'))
                            <span class="text-danger" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <div for="exampleFormControlSelect1">Select Tags</div>

                        {{--<select class="form-control js-example-basic-multiple"--}}
                        {{--multiple style="width: 100%" name="tags[]">--}}
                        {{--<option value="" hidden></option>--}}
                        @foreach($tags as $tag)
                            <label class="checkbox-inline">
                                <input type="checkbox" value="{{ $tag->id }}" name="tags[]"> {{ $tag->name }}
                            </label>
                            {{--<option value="{{$tag->id}}">{{$tag->name}}</option>--}}
                        @endforeach
                        {{--</select>--}}
                        @if ($errors->has('tags'))
                            <div class="text-danger" role="alert">
                                <strong>{{ $errors->first('tags') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlFile1">Post Image</label>
                        <input type="file" class="form-control-file" id="" name="image">
                        @if ($errors->has('image'))
                            <span class="text-danger" role="alert">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success col-sm-2 float-right mb-2">Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

{{-- This section is for special scripts--}}
@section("script")
    <script src="{{asset("assets/frontend/plugins/select2/js/select2.min.js")}}"></script>
     <script>
        $('.js-example-basic-multiple').select2({
            placeholder: 'Assign Users'
        });
    </script>
@endsection