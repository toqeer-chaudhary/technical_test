<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
//Route::get('/home,/', 'HomeController@home')->name('home');

// Main page
Route::get('/', 'HomeController@home')->name('home');
// Routes for UserController
Route::resource('/user', 'UserController');
// Routes for PostController
Route::resource('/post', 'PostController');
// Routes for TagController
Route::resource('/tag', 'TagController');