<?php

use Illuminate\Database\Seeder;
use App\Models\Post;
use App\Models\Tag;

class PostAndTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createPostAndTag();
    }

    public function createPostAndTag()
    {
            DB::table('post_tag')->insert([
                "id"         => 1,
                'post_id'    => 1,
                'tag_id'    => 1,
                'created_at' => now(),
            ]);

        DB::table('post_tag')->insert([
            "id"         => 2,
            'post_id'    => 1,
            'tag_id'    => 2,
            'created_at' => now(),
        ]);

        DB::table('post_tag')->insert([
            "id"         => 3,
            'post_id'    => 1,
            'tag_id'    => 3,
            'created_at' => now(),
        ]);

        DB::table('post_tag')->insert([
            "id"         => 4,
            'post_id'    => 2,
            'tag_id'    => 1,
            'created_at' => now(),
        ]);

        DB::table('post_tag')->insert([
            "id"         => 5,
            'post_id'    => 2,
            'tag_id'    => 4,
            'created_at' => now(),
        ]);

        DB::table('post_tag')->insert([
            "id"         => 6,
            'post_id'    => 3,
            'tag_id'    => 1,
            'created_at' => now(),
        ]);

        DB::table('post_tag')->insert([
            "id"         => 7,
            'post_id'    => 3,
            'tag_id'    => 3,
            'created_at' => now(),
        ]);

    }
}
