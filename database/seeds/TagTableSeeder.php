<?php

use App\Models\Tag;
use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertTags();
    }

    // this function will create dummy tags
    public function insertTags()
    {
        Tag::insert([
            'id' => 1,
            'user_id' => 1,
            'name' => "PHP",
            'created_at' => now(),
            'updated_at' => null,
        ]);

        Tag::insert([
            'id' => 2,
            'user_id' => 1,
            'name' => "AJAX",
            'created_at' => now(),
            'updated_at' => null,
        ]);

        Tag::insert([
            'id' => 3,
            'user_id' => 1,
            'name' => "Laravel",
            'created_at' => now(),
            'updated_at' => null,
        ]);

        Tag::insert([
            'id' => 4,
            'user_id' => 1,
            'name' => "Jquery",
            'created_at' => now(),
            'updated_at' => null,
        ]);

        Tag::insert([
            'id' => 5,
            'user_id' => 1,
            'name' => "JavaScript",
            'created_at' => now(),
            'updated_at' => null,
        ]);
    }
}
