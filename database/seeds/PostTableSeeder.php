<?php

use App\Models\Post;
use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertPosts();
    }

    // this function will create dummy posts
    public function insertPosts()
    {
        Post::insert([
            'id' => 1,
            'user_id' => 1,
            'title' => "What is Lorem Ipsum?",
            'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
            'image' => "1.jpg",
            'created_at' => now(),
            'updated_at' => null,
        ]);

        Post::insert([
            'id' => 2,
            'user_id' => 1,
            'title' => "Where does lorem ipsum come from?",
            'description' => "ontrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia.",
            'image' => "2.jpg",
            'created_at' => now(),
            'updated_at' => null,
        ]);

        Post::insert([
            'id' => 3,
            'user_id' => 1,
            'title' => "Why do we use lorem ipsum?",
            'description' => "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using.",
            'image' => "3.jpg",
            'created_at' => now(),
            'updated_at' => null,
        ]);
    }
}
